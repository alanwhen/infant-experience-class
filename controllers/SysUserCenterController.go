package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/enums"
	"bitbucket.org/alanwhen/infant-experience-class/helpers"
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"github.com/astaxie/beego/orm"
	"golang.org/x/crypto/bcrypt"
	"strings"
)

type SysUserCenterController struct {
	BaseController
}

func (this *SysUserCenterController) Prepare() {
	//先执行
	this.BaseController.Prepare()
	//如果一个Controller的所有Action都需要登录验证，则将验证放到Prepare
	this.checkLogin()
}

func (this *SysUserCenterController) Profile() {
	Id := this.curUser.Id
	m, err := models.SysUserOne(Id)
	if m == nil || err != nil {
		this.pageError("数据无效，请刷新后重试")
	}
	this.Data["hasAvatar"] = len(m.Avatar) > 0
	helpers.LogDebug(m.Avatar)

	this.Data["m"] = m
	this.setTpl("sys_user_center/profile")
}

func (this *SysUserCenterController) BasicInfoSave() {
	Id := this.curUser.Id
	oM, err := models.SysUserOne(Id)
	if oM == nil || err != nil {
		this.jsonResult(enums.JRCodeFailed, "数据无效，请刷新后重试", "")
	}
	m := models.SysUser{}

	//获取form里的值
	if err = this.ParseForm(&m); err != nil {
		this.jsonResult(enums.JRCodeFailed, "获取数据失败", m.Id)
	}

	oM.RealName = m.RealName
	oM.Mobile = m.Mobile
	oM.Email = m.Email
	oM.Avatar = this.GetString("ImageUrl")
	if len(oM.Avatar) == 0 {
		oM.Avatar = "/static/upload/tigger.png"
	}

	o := orm.NewOrm()
	if _, err := o.Update(oM); err != nil {
		this.jsonResult(enums.JRCodeFailed, "编辑失败", m.Id)
	} else {
		this.setSysUser2Session(Id)
		this.jsonResult(enums.JRCodeSuccess, "保存成功", m.Id)
	}
}

func (this *SysUserCenterController) PasswordSave() {
	Id := this.curUser.Id
	oM, err := models.SysUserOne(Id)
	if oM == nil || err != nil {
		this.pageError("数据无效，请刷新后重试")
	}
	oldPwd := strings.TrimSpace(this.GetString("Password", ""))
	newPwd := strings.TrimSpace(this.GetString("NewUserPwd", ""))
	confirmPwd := strings.TrimSpace(this.GetString("ConfirmPwd", ""))

	err = bcrypt.CompareHashAndPassword([]byte(oM.Password), []byte(oldPwd))
	if err != nil {
		this.jsonResult(enums.JRCodeFailed, "原密码错误", "")
	}

	if len(newPwd) == 0 {
		this.jsonResult(enums.JRCodeFailed, "请输入新密码", "")
	}

	if newPwd != confirmPwd {
		this.jsonResult(enums.JRCodeFailed, "两次输入的新密码不一致", "")
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(newPwd), bcrypt.DefaultCost)
	if err != nil {
		this.jsonResult(enums.JRCodeFailed, "密码生成错误", "")
	}

	oM.Password = string(hash)
	o := orm.NewOrm()
	if _, err := o.Update(oM); err != nil {
		this.jsonResult(enums.JRCodeFailed, "保存失败", oM.Id)
	} else {
		this.setSysUser2Session(Id)
		this.jsonResult(enums.JRCodeSuccess, "保存成功", oM.Id)
	}
}

func (this *SysUserCenterController) UploadImage() {
	//这里type没有用，只是为了演示传值
	stype, _ := this.GetInt32("type", 0)
	if stype > 0 {
		f, h, err := this.GetFile("fileImageUrl")
		if err != nil {
			this.jsonResult(enums.JRCodeFailed, "上传失败", "")
		}
		defer f.Close()

		filePath := "static/upload/" + h.Filename
		// 保存位置在 static/upload, 没有文件夹要先创建
		this.SaveToFile("fileImageUrl", filePath)
		this.jsonResult(enums.JRCodeSuccess, "上传成功", "/"+filePath)
	} else {
		this.jsonResult(enums.JRCodeFailed, "上传失败", "")
	}
}
