package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/enums"
	"bitbucket.org/alanwhen/infant-experience-class/helpers"
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"fmt"
	"github.com/astaxie/beego"
	"reflect"
	"strings"
)

type BaseController struct {
	beego.Controller
	controllerName string
	actionName     string
	curUser        models.SysUser
}

func (this *BaseController) Prepare() {
	this.controllerName, this.actionName = this.GetControllerAndAction()

	this.Data["siteApp"] = beego.AppConfig.String("site.app")
	this.Data["siteName"] = beego.AppConfig.String("site.name")
	this.Data["siteVersion"] = beego.AppConfig.String("site.version")

	//从Session里获取数据 设置用户信息
	this.adapterUserInfo()
}

func (this *BaseController) adapterUserInfo() {
	a := this.GetSession("sys_user")
	if a != nil {
		this.curUser = a.(models.SysUser)
		this.Data["sys_user"] = a
	}
}

// checkLogin判断用户是否登录，未登录则跳转至登录页面
// 一定要在BaseController.Prepare()后执行
func (this *BaseController) checkLogin() {
	if this.curUser.Id == 0 {
		//登录页面地址
		urlstr := this.URLFor("HomeController.Login") + "?url="

		//登录成功后返回的址为当前
		returnURL := this.Ctx.Request.URL.Path

		//如果ajax请求则返回相应的错码和跳转的地址
		if this.Ctx.Input.IsAjax() {
			//由于是ajax请求，因此地址是header里的Referer
			returnURL := this.Ctx.Input.Refer()
			this.jsonResult(enums.JRCode302, "请登录", urlstr+returnURL)
		}
		this.Redirect(urlstr+returnURL, 302)
		this.StopRun()
	}
}

// 判断某 Controller.Action 当前用户是否有权访问
func (this *BaseController) checkActionAuthor(ctrlName, ActName string) bool {
	if this.curUser.Id == 0 {
		return false
	}
	if !strings.Contains(ctrlName, "Controller") {
		ctrlName = ctrlName + "Controller"
	}

	//从session获取用户信息
	user := this.GetSession("sys_user")

	//类型断言
	v, ok := user.(models.SysUser)
	if ok {
		//如果是超级管理员，则直接通过
		if v.IsSuper == true {
			return true
		}

		//遍历用户所负责的资源列表
		for i, _ := range v.ResourceUrlForList {
			urlfor := strings.TrimSpace(v.ResourceUrlForList[i])
			if len(urlfor) == 0 {
				continue
			}
			// TestController.Get,:last,xie,:first,asta
			strs := strings.Split(urlfor, ",")
			if len(strs) > 0 && strs[0] == (ctrlName+"."+ActName) {
				return true
			}
		}
	}
	return false
}

// checkLogin判断用户是否有权访问某地址，无权则会跳转到错误页面
//一定要在BaseController.Prepare()后执行
// 会调用checkLogin
// 传入的参数为忽略权限控制的Action
func (this *BaseController) checkAuthor(ignores ...string) {
	//先判断是否登录
	this.checkLogin()

	//如果Action在忽略列表里，则直接通用
	for _, ignore := range ignores {
		if ignore == this.actionName {
			return
		}
	}

	hasAuthor := this.checkActionAuthor(this.controllerName, this.actionName)
	if !hasAuthor {
		helpers.LogDebug(fmt.Sprintf("author control: path=%s.%s userid=%v  无权访问", this.controllerName, this.actionName, this.curUser.Id))

		//如果没有权限
		if !hasAuthor {
			if this.Ctx.Input.IsAjax() {
				this.jsonResult(enums.JRCode401, "无权访问", "")
			} else {
				this.pageError("无权访问")
			}
		}
	}
}

//SetSysUser2Session 获取用户信息（包括资源UrlFor）保存至Session
//被 HomeController.DoLogin 调用
func (this *BaseController) setSysUser2Session(userId int) error {
	m, err := models.SysUserOne(userId)
	if err != nil {
		return err
	}

	//获取这个用户能获取到的所有资源列表
	resourceList := models.ResourceTreeGridByUserId(userId, 1000)
	for _, item := range resourceList {
		m.ResourceUrlForList = append(m.ResourceUrlForList, strings.TrimSpace(item.UrlFor))
	}
	this.SetSession("sys_user", *m)
	return nil
}

// 设置模板
// 第一个参数模板，第二个参数为layout
func (this *BaseController) setTpl(template ...string) {
	layout := "shared/layout_page.html"
	extension := beego.AppConfig.String("view_extension")
	display := "show"

	var tplName string
	switch {
	case len(template) == 1:
		tplName = template[0]
	case len(template) == 2:
		tplName = template[0]
		layout = template[1]
	case len(template) == 3:
		tplName = template[0]
		layout = template[1]
		display = template[2]
	default:
		//不要Controller这个10个字母
		ctrlName := strings.ToLower(this.controllerName[0 : len(this.controllerName)-10])
		actionName := strings.ToLower(this.actionName)
		tplName = ctrlName + "/" + actionName
	}

	tplName = tplName + extension
	if strings.Index(layout, extension) <= -1 {
		layout = layout + extension
	}
	//this.jsonResult(enums.JRCodeFailed, "", layout)
	this.Layout = layout
	this.TplName = tplName

	if display == "show" {
		this.LayoutSections = make(map[string]string)
		this.LayoutSections["header"] = strings.Replace(tplName, extension, "", 1) + "_header" + extension
		this.LayoutSections["footer"] = strings.Replace(tplName, extension, "", 1) + "_footer" + extension
	}
	if display == "header" {
		this.LayoutSections = make(map[string]string)
		this.LayoutSections["header"] = strings.Replace(tplName, extension, "", 1) + "_header" + extension
	}
	if display == "footer" {
		this.LayoutSections = make(map[string]string)
		this.LayoutSections["footer"] = strings.Replace(tplName, extension, "", 1) + "_footer" + extension
	}
}

func (this *BaseController) ActiveSidebarUrl() {
	this.Data["activeSidebarUrl"] = this.URLFor(this.controllerName + "." + this.actionName)
}

func (this *BaseController) jsonResult(code enums.JsonResultCode, msg string, obj interface{}) {
	res := &models.JsonResult{Code: code, Msg: msg, Obj: obj}
	this.Data["json"] = res
	this.ServeJSON()
	this.StopRun()
}

// 重定向
func (this *BaseController) redirect(url string) {
	this.Redirect(url, 302)
	this.StopRun()
}

// 重定向 去错误页
func (this *BaseController) pageError(msg string) {
	errorurl := this.URLFor("HomeController.Error") + "/" + msg
	this.Redirect(errorurl, 302)
	this.StopRun()
}

// 重定向 去登录页
func (this *BaseController) pageLogin() {
	url := this.URLFor("HomeController.Login")
	this.Redirect(url, 302)
	this.StopRun()
}

func (this *BaseController) BootstrapJson(total int64, rows interface{}) {
	result := make(map[string]interface{})
	result["total"] = total
	result["rows"] = rows
	this.Data["json"] = result
	this.ServeJSON()
}

func (u *BaseController) Merge(a, b interface{}) (*interface{}, error) {
	var result interface{}

	aValOf := reflect.ValueOf(a)
	bValOf := reflect.ValueOf(b)

	resultValOf := reflect.ValueOf(result)
	aValues := make([]interface{}, aValOf.NumField())
	resultValues := make([]interface{}, resultValOf.NumField())

	for i := 0; i < aValOf.NumField(); i++ {
		if reflect.ValueOf(aValues[i]).IsNil() {
			resultValues[i] = bValOf.Field(i).Interface()
			break
		}
		resultValues[i] = aValOf.Field(i).Interface()
	}
	return &result, nil
}
