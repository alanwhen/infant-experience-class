package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/enums"
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
)

type StudentController struct {
	BaseController
}

func (u *StudentController) Prepare() {
	u.BaseController.Prepare()
	u.checkAuthor("DataGrid")
}

func (u *StudentController) Index() {
	u.checkLogin()
	u.Data["pageTitle"] = "学籍卡列表"
	u.ActiveSidebarUrl()
	u.Data["canEdit"] = u.checkActionAuthor(u.controllerName, "Edit")
	u.Data["canDelete"] = u.checkActionAuthor(u.controllerName, "Delete")
	u.setTpl("student/index")
}

func (u *StudentController) DataGrid() {
	var params models.StudentQueryParam
	json.Unmarshal(u.Ctx.Input.RequestBody, &params)

	data, total := models.StudentPageList(&params)
	u.BootstrapJson(total, data)
}

func (u *StudentController) Edit() {
	if u.Ctx.Request.Method == "POST" {
		u.Save()
	}
	Id, _ := u.GetInt(":id", 0)
	m := &models.Student{}
	var err error
	if Id > 0 {
		m, err = models.StudentOne(Id)
		if err != nil {
			u.pageError("数据无效，请刷新后重试")
		}
	}
	u.Data["m"] = m
	u.setTpl("student/edit", "shared/layout_pullbox")
}

func (u *StudentController) Save() {
	m := models.Student{}
	o := orm.NewOrm()
	var err error

	if err = u.ParseForm(&m); err != nil {
		u.jsonResult(enums.JRCodeFailed, "获取失败", m.Id)
	}
	m.UpdateAt = time.Now().Format("2006-01-02 15:04:05")
	if m.Id == 0 {
		m.CreateAt = time.Now().Format("2006-01-02 15:04:05")
		if _, err := o.Insert(&m); err != nil {
			u.jsonResult(enums.JRCodeFailed, "添加失败", m.Id)
		}
	} else {
		oM, err := models.StudentOne(m.Id)
		if err != nil {
			u.jsonResult(enums.JRCodeFailed, "数据无效，请稍后重试", m.Id)
		} else {
			m.CreateAt = oM.CreateAt
			if _, err := o.Update(&m); err != nil {
				u.jsonResult(enums.JRCodeFailed, "编辑失败", m.Id)
			}
		}
	}
	u.jsonResult(enums.JRCodeSuccess, "保存成功", m.Id)
}

func (u *StudentController) Delete() {
	str := u.GetString("ids")
	ids := make([]int, 0, len(str))

	for _, item := range strings.Split(str, ",") {
		if id, err := strconv.Atoi(item); err == nil {
			ids = append(ids, id)
		}
	}

	query := orm.NewOrm().QueryTable(models.StudentTBName())
	if num, err := query.Filter("id__in", ids).Delete(); err == nil {
		u.jsonResult(enums.JRCodeSuccess, fmt.Sprintf("删除成功%d项", num), num)
	} else {
		u.jsonResult(enums.JRCodeFailed, fmt.Sprintf("删除失败%d", num), num)
	}
}
