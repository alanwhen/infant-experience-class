package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/enums"
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
)

type NotificationCategoryController struct {
	BaseController
}

func (u *NotificationCategoryController) Prepare() {
	u.BaseController.Prepare()
	u.checkAuthor("DataGrid")
}

func (u *NotificationCategoryController) Index() {
	u.checkLogin()
	u.Data["pageTitle"] = "通知类型"
	u.ActiveSidebarUrl()
	u.Data["canEdit"] = u.checkActionAuthor(u.controllerName, "Edit")
	u.Data["canDelete"] = u.checkActionAuthor(u.controllerName, "Delete")
	u.setTpl("notification_category/index")
}

func (u *NotificationCategoryController) DataGrid() {
	var params models.NotificationCategoryQueryParam
	json.Unmarshal(u.Ctx.Input.RequestBody, &params)

	data, total := models.NotificationCategoryPageList(&params)
	u.BootstrapJson(total, data)
}

func (u *NotificationCategoryController) DataList() {
	var params = models.NotificationCategoryQueryParam{}

	//获取数据列表和总数
	data := models.NotificationCategoryDataList(&params)

	//定义返回的数据结构
	u.jsonResult(enums.JRCodeSuccess, "", data)
}

func (u *NotificationCategoryController) Edit() {
	if u.Ctx.Request.Method == "POST" {
		u.Save()
	}
	Id, _ := u.GetInt(":id", 0)
	m := &models.NotificationCategory{Id: Id}
	var err error
	if Id > 0 {
		m, err = models.NotificationCategoryOne(Id)
		if err != nil {
			u.pageError("数据无效， 请刷新后重试" + err.Error())
		}
	} else {

	}
	u.Data["m"] = m

	u.setTpl("notification_category/edit", "shared/layout_pullbox")
}

func (u *NotificationCategoryController) Save() {
	m := models.NotificationCategory{}
	o := orm.NewOrm()
	var err error

	if err = u.ParseForm(&m); err != nil {
		u.jsonResult(enums.JRCodeFailed, "获取数据失败", m.Id)
	}

	if m.Id == 0 {
		m.CreateAt = time.Now().Unix()
		if _, err := o.Insert(&m); err != nil {
			u.jsonResult(enums.JRCodeFailed, "添加失败"+err.Error(), m.Id)
		}
	} else {
		oM, err := models.NotificationCategoryOne(m.Id)
		if err != nil {
			u.jsonResult(enums.JRCodeFailed, "数据无效，请刷新后重试"+err.Error(), m.Id)
		} else {
			m.CreateAt = oM.CreateAt
			if _, err := o.Update(&m); err != nil {
				u.jsonResult(enums.JRCodeFailed, "编辑失败", m.Id)
			}
		}
	}
	u.jsonResult(enums.JRCodeSuccess, "保存成功", m.Id)
}

func (u *NotificationCategoryController) Delete() {
	str := u.GetString("ids")
	ids := make([]int, 0, len(str))

	for _, item := range strings.Split(str, ",") {
		if id, err := strconv.Atoi(item); err == nil {
			ids = append(ids, id)
		}
	}

	query := orm.NewOrm().QueryTable(models.NotificationCategoryTBName())
	if _, err := query.Filter("id__in", ids).Delete(); err == nil {
		u.jsonResult(enums.JRCodeSuccess, fmt.Sprintf("删除成功"), 0)
	} else {
		u.jsonResult(enums.JRCodeFailed, "删除失败", 0)
	}
}
