package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/enums"
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
)

type ArticleContentController struct {
	BaseController
}

func (u *ArticleContentController) Prepare() {
	u.BaseController.Prepare()
	u.checkAuthor("DataGrid")
}

func (u *ArticleContentController) Index() {
	u.checkLogin()
	u.Data["pageTitle"] = "内容管理"
	u.Data["canEdit"] = u.checkActionAuthor(u.controllerName, "Edit")
	u.Data["canDelete"] = u.checkActionAuthor(u.controllerName, "Delete")
	u.ActiveSidebarUrl()
	u.setTpl("content/index")
}

func (u *ArticleContentController) DataGrid() {
	var params models.ContentQueryParam
	json.Unmarshal(u.Ctx.Input.RequestBody, &params)

	data, total := models.ContentPageList(&params)

	u.BootstrapJson(total, data)
}

func (u *ArticleContentController) Edit() {
	if u.Ctx.Request.Method == "POST" {
		u.Save()
	}
	Id, _ := u.GetInt(":id", 0)
	m := &models.Content{}
	var err error
	if Id > 0 {
		m, err = models.ContentOne(Id)
		if err != nil {
			u.pageError("数据无效，请刷新后重试")
		}
	}
	u.Data["m"] = m

	u.setTpl("content/edit", "shared/layout_pullbox")
}

func (u *ArticleContentController) Save() {
	m := models.Content{}
	o := orm.NewOrm()
	var err error

	if err = u.ParseForm(&m); err != nil {
		u.jsonResult(enums.JRCodeFailed, "获取数据失败"+err.Error(), m.Id)
	}
	m.UpdateAt = time.Now().Unix()

	if m.Id == 0 {
		m.CreateAt = time.Now().Unix()
		if _, err := o.Insert(&m); err != nil {
			u.jsonResult(enums.JRCodeFailed, "添加失败"+err.Error(), m.Id)
		}
	} else {
		oM, err := models.ContentOne(m.Id)
		if err != nil {
			u.jsonResult(enums.JRCodeFailed, "数据无效，请刷新后重试", m.Id)
		} else {
			m.CreateAt = oM.CreateAt
			if _, err := o.Update(&m); err != nil {
				u.jsonResult(enums.JRCodeFailed, "编辑失败", m.Id)
			}
		}
	}
	u.jsonResult(enums.JRCodeSuccess, "保存成功", m.Id)
}

func (u *ArticleContentController) Delete() {
	str := u.GetString("ids")
	ids := make([]int, 0, len(str))

	for _, item := range strings.Split(str, ",") {
		if id, err := strconv.Atoi(item); err == nil {
			ids = append(ids, id)
		}
	}

	query := orm.NewOrm().QueryTable(models.ContentTBName())
	if _, err := query.Filter("id__in", ids).Delete(); err == nil {
		u.jsonResult(enums.JRCodeSuccess, fmt.Sprintf("删除成功"), 0)
	} else {
		u.jsonResult(enums.JRCodeFailed, "删除失败", 0)
	}
}
