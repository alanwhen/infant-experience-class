package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/enums"
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"encoding/json"
)

type LoginTraceController struct {
	BaseController
}

func (this *LoginTraceController) Prepare() {
	this.BaseController.Prepare()
	this.checkAuthor("DataGrid", "DataList")
}

func (this *LoginTraceController) Index() {
	this.Data["pageTitle"] = "用户登录日志"
	this.Data["showMoreQuery"] = false
	this.ActiveSidebarUrl()
	this.setTpl("login_trace/index")
}

func (this *LoginTraceController) DataGrid() {
	var params models.LoginTraceQueryParam
	json.Unmarshal(this.Ctx.Input.RequestBody, &params)
	data, total := models.LoginTracePageList(&params)

	result := make(map[string]interface{})
	result["total"] = total
	result["rows"] = data

	this.Data["json"] = result
	this.ServeJSON()
}

func (this *LoginTraceController) DataList() {
	var params = models.LoginTraceQueryParam{}
	data := models.LoginTraceDataList(&params)
	this.jsonResult(enums.JRCodeSuccess, "", data)
}
