package controllers

import (
	"bitbucket.org/alanwhen/infant-experience-class/models"
	"encoding/json"
)

type WeChatController struct {
	BaseController
}

func (u *WeChatController) Prepare() {
	u.BaseController.Prepare()
}

func (u *WeChatController) Index() {
	u.checkLogin()
	u.Data["pageTitle"] = "微信用户"
	u.ActiveSidebarUrl()
	u.setTpl("wechat/index")
}

func (u *WeChatController) DataGrid() {
	var params models.WeChatQueryParam
	json.Unmarshal(u.Ctx.Input.RequestBody, &params)

	data, total := models.WeChatPageList(&params)

	u.BootstrapJson(total, data)
}
