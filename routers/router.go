package routers

import (
	"bitbucket.org/alanwhen/infant-experience-class/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.HomeController{}, "*:Index")

	//home
	beego.Router("/home/index", &controllers.HomeController{}, "*:Index")
	beego.Router("/home/login", &controllers.HomeController{}, "*:Login")
	beego.Router("/home/do_login", &controllers.HomeController{}, "Post:DoLogin")
	beego.Router("/home/logout", &controllers.HomeController{}, "*:Logout")
	beego.Router("/home/404", &controllers.HomeController{}, "*:Page404")
	beego.Router("/home/error/?:error", &controllers.HomeController{}, "*:Error")

	//sys_user
	//后台用户路由
	beego.Router("/sys_user/index", &controllers.SysUserController{}, "*:Index")
	beego.Router("/sys_user/data_grid", &controllers.SysUserController{}, "POST:DataGrid")
	beego.Router("/sys_user/edit/?:id", &controllers.SysUserController{}, "Get,Post:Edit")
	beego.Router("/sys_user/delete", &controllers.SysUserController{}, "Post:Delete")

	//role
	//用户角色
	beego.Router("/role/index", &controllers.RoleController{}, "*:Index")
	beego.Router("/role/data_grid", &controllers.RoleController{}, "Get,Post:DataGrid")
	beego.Router("/role/edit/?:id", &controllers.RoleController{}, "Get,Post:Edit")
	beego.Router("/role/delete", &controllers.RoleController{}, "Post:Delete")
	beego.Router("/role/data_list", &controllers.RoleController{}, "Post:DataList")
	beego.Router("/role/allocate", &controllers.RoleController{}, "Post:Allocate")
	beego.Router("/role/update_seq", &controllers.RoleController{}, "Post:UpdateSeq")

	//资源路由
	beego.Router("/resource/index", &controllers.ResourceController{}, "*:Index")
	beego.Router("/resource/tree_grid", &controllers.ResourceController{}, "POST:TreeGrid")
	beego.Router("/resource/edit/?:id", &controllers.ResourceController{}, "Get,Post:Edit")
	beego.Router("/resource/parent", &controllers.ResourceController{}, "Post:ParentTreeGrid")
	beego.Router("/resource/delete", &controllers.ResourceController{}, "Post:Delete")

	//快速修改顺序
	beego.Router("/resource/update_seq", &controllers.ResourceController{}, "Post:UpdateSeq")

	//通用选择面板
	beego.Router("/resource/select", &controllers.ResourceController{}, "Get:Select")
	beego.Router("/resource/chooseIcon", &controllers.ResourceController{}, "Get:ChooseIcon")

	//用户有权管理的菜单列表（包括区域）
	beego.Router("/resource/user_menu_tree", &controllers.ResourceController{}, "POST:UserMenuTree")
	beego.Router("/resource/check_url_for", &controllers.ResourceController{}, "POST:CheckUrlFor")

	//后台用户中心
	beego.Router("/sys_user_center/profile", &controllers.SysUserCenterController{}, "Get:Profile")
	beego.Router("/sys_user_center/basic_info_save", &controllers.SysUserCenterController{}, "Post:BasicInfoSave")
	beego.Router("/sys_user_center/upload_image", &controllers.SysUserCenterController{}, "Post:UploadImage")
	beego.Router("/sys_user_center/password_save", &controllers.SysUserCenterController{}, "Post:PasswordSave")

	//注册用户
	beego.Router("/register_user/index", &controllers.RegisterUserController{}, "*:Index")
	beego.Router("/register_user/data_grid", &controllers.RegisterUserController{}, "POST:DataGrid")
	beego.Router("/register_user/edit/?:id", &controllers.RegisterUserController{}, "Get,Post:Edit")
	beego.Router("/register_user/delete", &controllers.RegisterUserController{}, "Post:Delete")

	beego.Router("/course/index", &controllers.CourseController{}, "*:Index")
	beego.Router("/course/data_grid", &controllers.CourseController{}, "POST:DataGrid")
	beego.Router("/course/edit/?:id", &controllers.CourseController{}, "Get,Post:Edit")
	beego.Router("/course/delete", &controllers.CourseController{}, "Post:Delete")
	beego.Router("/course/get_address", &controllers.CourseController{}, "POST:GetAddress")
	beego.Router("/course/get_suggestion", &controllers.CourseController{}, "POST:GetSuggestionAddress")
	beego.Router("/course/get_city_data", &controllers.CourseController{}, "Get:GetCityData")
	beego.Router("/course/setting_tab/?:id", &controllers.CourseController{}, "Get,Post:SettingTab")

	//上传公用
	beego.Router("/attachment/upload_minio", &controllers.AttachmentController{}, "POST:UploadMinio")
	beego.Router("/attachment/upload_file", &controllers.AttachmentController{}, "POST:UploadFile")

	beego.Router("/course_content/index", &controllers.CourseContentController{}, "*:Index")
	beego.Router("/course_content/datagrid", &controllers.CourseContentController{}, "POST:DataGrid")
	beego.Router("/course_content/edit/?:id", &controllers.CourseContentController{}, "Get,Post:Edit")
	beego.Router("/course_content/delete", &controllers.CourseContentController{}, "Post:Delete")
	beego.Router("/course_content/match/?:id", &controllers.CourseContentController{}, "Get,Post:Match")
	beego.Router("/course_content/item_data", &controllers.CourseContentController{}, "Get:ItemData")

	beego.Router("/course_apply/index", &controllers.CourseApplyController{}, "*:Index")
	beego.Router("/course_apply/data_grid", &controllers.CourseApplyController{}, "POST:DataGrid")

	//通知
	beego.Router("/notification/index", &controllers.NotificationController{}, "*:Index")
	beego.Router("/notification/edit/?:id", &controllers.NotificationController{}, "Get,Post:Edit")
	beego.Router("/notification/delete", &controllers.NotificationController{}, "Post:Delete")
	beego.Router("notification/data_grid", &controllers.NotificationController{}, "POST:DataGrid")

	beego.Router("/notification_category/index", &controllers.NotificationCategoryController{}, "*:Index")
	beego.Router("/notification_category/edit/?:id", &controllers.NotificationCategoryController{}, "Get,Post:Edit")
	beego.Router("/notification_category/data_grid", &controllers.NotificationCategoryController{}, "Post:DataGrid")
	beego.Router("/notification_category/delete", &controllers.NotificationCategoryController{}, "Post:Delete")
	beego.Router("/notification_category/data_list", &controllers.NotificationCategoryController{}, "Post:DataList")

	beego.Router("/wechat/index", &controllers.WeChatController{}, "*:Index")
	beego.Router("/wechat/data_grid", &controllers.WeChatController{}, "Post:DataGrid")

	beego.Router("/course_comment/index", &controllers.CourseCommentController{}, "*:Index")
	beego.Router("/course_comment/data_grid", &controllers.CourseCommentController{}, "POST:DataGrid")
	beego.Router("/course_comment/delete", &controllers.CourseCommentController{}, "Post:Delete")
	beego.Router("/course_comment/audit", &controllers.CourseCommentController{}, "Post:Audit")

	beego.Router("/article_content/index", &controllers.ArticleContentController{}, "*:Index")
	beego.Router("/article_content/edit/?:id", &controllers.ArticleContentController{}, "Get,Post:Edit")
	beego.Router("/article_content/data_grid", &controllers.ArticleContentController{}, "POST:DataGrid")
	beego.Router("/article_content/delete", &controllers.ArticleContentController{}, "POST:Delete")

	beego.Router("/article_category/index", &controllers.ArticleCategoryController{}, "*:Index")
	beego.Router("/article_category/edit/?:id", &controllers.ArticleCategoryController{}, "Get,Post:Edit")
	beego.Router("/article_category/data_grid", &controllers.ArticleCategoryController{}, "Post:DataGrid")
	beego.Router("/article_category/delete", &controllers.ArticleCategoryController{}, "Post:Delete")

	beego.Router("/student/index", &controllers.StudentController{}, "*:Index")
	beego.Router("/student/edit/?:id", &controllers.StudentController{}, "Get,Post:Edit")
	beego.Router("/student/data_grid", &controllers.StudentController{}, "Post:DataGrid")
	beego.Router("/student/delete", &controllers.StudentController{}, "Post:Delete")
}
