FROM golang:latest

WORKDIR $GOPATH/src/bitbucket.org/alanwhen/infant-experience-class
COPY . .

RUN tar zxf src.tar.gz -C $GOPATH/ && \
 rm src.tar.gz && \
 sed -e '/\[system_default_sect\]/d;/MinProtocol = TLSv1.2/d;/CipherString = DEFAULT@SECLEVEL=2/d' /etc/ssl/openssl.cnf > /etc/ssl/openssl.cnf
#WORKDIR $GOPATH/src/arvonblog
#
RUN go get

RUN go build ./main.go

EXPOSE 8088

ENTRYPOINT ["./main"]
##