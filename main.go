package main

import (
	_ "bitbucket.org/alanwhen/infant-experience-class/routers"
	_ "bitbucket.org/alanwhen/infant-experience-class/sysinit"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
