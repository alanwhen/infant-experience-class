package models

import (
	"github.com/astaxie/beego/orm"
	"time"
)

type RoleResourceRel struct {
	Id       int
	Role     *Role     `orm:"rel(fk)"`  //外键
	Resource *Resource `orm:"rel(fk)" ` //外键
	Created  time.Time `orm:"auto_now_add;type(datetime)"`
}

func init() {
	orm.RegisterModel(new(RoleResourceRel))
}

func RoleResourceRelTBName() string {
	return TableName("sys_role_resource_rel")
}

func (this *RoleResourceRel) TableName() string {
	return RoleResourceRelTBName()
}
