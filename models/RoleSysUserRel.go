package models

import (
	"github.com/astaxie/beego/orm"
	"time"
)

type RoleSysUserRel struct {
	Id      int
	Role    *Role     `orm:"rel(fk)"`  //外键
	SysUser *SysUser  `orm:"rel(fk)" ` // 外键
	Created time.Time `orm:"auto_now_add;type(datetime)"`
}

func init() {
	orm.RegisterModel(new(RoleSysUserRel))
}

func RoleSysUserRelTBName() string {
	return TableName("sys_role_sys_user_rel")
}

func (this *RoleSysUserRel) TableName() string {
	return RoleSysUserRelTBName()
}
