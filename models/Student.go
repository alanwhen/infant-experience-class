package models

import (
	"github.com/astaxie/beego/orm"
)

type StudentQueryParam struct {
	BaseQueryParam
}

type Student struct {
	Id          int
	UserId      int
	StudentName string
	Nickname    string
	Address     string
	Lat         string
	Lng         string
	Contact     string
	ContactNo   string
	CreateAt    string
	UpdateAt    string
	UpdateAgeAt string
	Age         int
}

func init() {
	orm.RegisterModel(new(Student))
}

func (u *Student) TableName() string {
	return StudentTBName()
}

func StudentTBName() string {
	return TableName("student")
}

func StudentPageList(params *StudentQueryParam) ([]*Student, int64) {
	query := orm.NewOrm().QueryTable(StudentTBName())
	data := make([]*Student, 0)
	sort := "Id"
	switch params.Sort {
	case "Id":
		sort = "Id"
	}

	if params.Order == "desc" {
		sort = "-" + sort
	}

	total, _ := query.Count()
	query.OrderBy(sort).Limit(params.Limit, params.Offset).All(&data)
	return data, total
}

func StudentOne(Id int) (*Student, error) {
	o := orm.NewOrm()
	m := Student{Id: Id}
	err := o.Read(&m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}
