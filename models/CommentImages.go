package models

import "github.com/astaxie/beego/orm"

type CommentImages struct {
	Id        int
	Comment   *Comment `orm:"rel(fk)"`
	ImagePath string
	Bucket    string
	Filename  string
}

func init() {
	orm.RegisterModel(new(CommentImages))
}

func (u *CommentImages) TableName() string {
	return CommentImagesTBName()
}

func CommentImagesTBName() string {
	return TableName("comment_images")
}
