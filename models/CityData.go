package models

import "github.com/astaxie/beego/orm"

type CityData struct {
	Id           int    `json:"id"`
	Code         int    `json:"code"`
	Name         string `json:"name"`
	ProvinceCode int    `json:"province_code"`
}

func init() {
	orm.RegisterModel(new(CityData))
}

func (this *CityData) TableName() string {
	return CityDataTBName()
}

func CityDataTBName() string {
	return TableName("city_data")
}

func GetCityDataList() []*CityData {
	query := orm.NewOrm().QueryTable(CityDataTBName())
	data := make([]*CityData, 0)
	query.All(&data)
	return data
}
