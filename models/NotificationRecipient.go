package models

import "github.com/astaxie/beego/orm"

type NotificationRecipient struct {
	Id           int
	Notification *Notification `orm:"rel(one)"`
	ReadTime     int
	UserType     int
	User         *User `orm:"rel(fk)"`
}

func init() {
	orm.RegisterModel(new(NotificationRecipient))
}

func (this *NotificationRecipient) TableName() string {
	return NotificationRecipientTBName()
}

func NotificationRecipientTBName() string {
	return TableName("notification_recipient")
}
