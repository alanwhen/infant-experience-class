package models

import "github.com/astaxie/beego/orm"

type CourseItem struct {
	Id       int
	CourseId int
	Title    string
	Thumb    string
	Items    string
}

func init() {
	orm.RegisterModel(new(CourseItem))
}

func (u *CourseItem) TableName() string {
	return CourseItemTBName()
}

func CourseItemTBName() string {
	return TableName("course_item")
}
