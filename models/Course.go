package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	url2 "net/url"
)

type CourseQueryParam struct {
	BaseQueryParam
	CourseName string
}

type AddressParam struct {
	Region  string `json:"region"`
	Address string `json:"address"`
}

type SuggestionParam struct {
	Keyword string `json:"keyword"`
	Region  string `json:"region"`
}

type Course struct {
	Id                int
	CourseName        string `orm:"column(course_name)" form:"CourseName" valid:"Required"`
	CourseDescription string `orm:"column(course_description)" form:"CourseDescription"`
	CourseDuration    int    `orm:"column(course_duration)" form:"CourseDuration"`
	Lat               string `orm:"column(lat)" form:"Lat"`
	Lng               string `orm:"column(lng)" form:"Lng"`
	AreaId            int    `orm:"column(area_id)" form:"AreaId"`
	Address           string `orm:"column(address)" form:"Address"`
	AddressName       string `orm:"column(address_name)" form:"AddressName"`
	ViewNumbers       int    `orm:"column(view_numbers)" form:"ViewNumbers"`
	CreateAt          int64  `orm:"column(create_at)" form:"CreateAt"`
	UpdateAt          int64  `orm:"column(update_at)" form:"UpdateAt"`
	StartTime         string `orm:"column(start_time)" form:"StartTime"`
	LessonTime        string `orm:"column(lesson_time)" form:"LessonTime"`
	Status            int    `orm:"column(status)" form:"Status"`
	CourseContentId   int    `orm:"column(course_content_id)"`
	IsRecommend       int    `orm:"column(is_recommend)"`
	SupportTimes      int    `orm:"column(support_times)"`
	Expire            int    `orm:"column(expire)"`
	Logo              string `orm:"column(logo)"`
	Background        string `orm:"column(background)"`
	DeadLine          string `orm:"column(dead_line)" form:"DeadLine"`
	ContactNumber     string
}

type SimpleCourse struct {
	Id         int
	CourseName string
}

func (this *Course) TableName() string {
	return CourseTBName()
}

func CourseTBName() string {
	return TableName("course")
}

func init() {
	orm.RegisterModel(new(Course))
}

func CourseOne(id int) (*Course, error) {
	o := orm.NewOrm()
	m := Course{Id: id}
	err := o.Read(&m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func GetGeocoderAddress(params *AddressParam) (string, error) {
	queryParam := url2.Values{}
	Url, err := url2.Parse("https://apis.map.qq.com/ws/geocoder/v1/")
	if err != nil {
		return "", err
	}
	fmt.Sprintln("获取到的地址值" + params.Address)
	queryParam.Set("key", "KWRBZ-IEWCX-DB64Y-7NWPE-RSFV2-ZVFDB")
	queryParam.Set("address", params.Address)
	if len(params.Region) > 0 {
		queryParam.Set("region", params.Region)
	}

	Url.RawQuery = queryParam.Encode()
	return Url.String(), err
}

func GetSuggestionAddress(params *SuggestionParam) (string, error) {
	queryParam := url2.Values{}
	Url, err := url2.Parse("https://apis.map.qq.com/ws/place/v1/suggestion/")
	if err != nil {
		return "", err
	}
	fmt.Sprintln("获取到的地址值" + params.Keyword)
	queryParam.Set("key", "KWRBZ-IEWCX-DB64Y-7NWPE-RSFV2-ZVFDB")
	queryParam.Set("keyword", params.Keyword)
	if len(params.Region) > 0 {
		queryParam.Set("region", params.Region)
	}

	Url.RawQuery = queryParam.Encode()
	return Url.String(), err
}

func CoursePageList(params *CourseQueryParam) ([]*Course, int64) {
	query := orm.NewOrm().QueryTable(CourseTBName())
	data := make([]*Course, 0)

	sort := "Id"
	switch params.Sort {
	case "Id":
		sort = "Id"
	}

	if params.Order == "desc" {
		sort = "-" + sort
	}

	if len(params.CourseName) > 0 {
		query = query.Filter("course_name__istartswith", params.CourseName)
	}

	total, _ := query.Count()
	query.OrderBy(sort).Limit(params.Limit, params.Offset).All(&data)
	return data, total
}
