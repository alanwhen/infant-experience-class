package models

import "github.com/astaxie/beego/orm"

type NotificationCategory struct {
	Id          int
	Name        string
	CreateAt    int64
	IfPatriarch int
	IfTeacher   int
	IfAdmin     int
}

type NotificationCategoryQueryParam struct {
	BaseQueryParam
}

func init() {
	orm.RegisterModel(new(NotificationCategory))
}

func (this *NotificationCategory) TableName() string {
	return NotificationCategoryTBName()
}

func NotificationCategoryTBName() string {
	return TableName("notification_category")
}

func NotificationCategoryOne(id int) (*NotificationCategory, error) {
	o := orm.NewOrm()
	m := NotificationCategory{Id: id}
	err := o.Read(&m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func NotificationCategoryPageList(params *NotificationCategoryQueryParam) ([]*NotificationCategory, int64) {
	query := orm.NewOrm().QueryTable(NotificationCategoryTBName())
	data := make([]*NotificationCategory, 0)

	sort := "Id"

	switch params.Sort {
	case "Id":
		sort = "Id"
	}

	if params.Order == "desc" {
		sort = "-" + sort
	}

	total, _ := query.Count()
	query.OrderBy(sort).Limit(params.Limit, params.Offset).All(&data)
	return data, total
}

//获取角色列表
func NotificationCategoryDataList(params *NotificationCategoryQueryParam) []*NotificationCategory {
	params.Limit = -1
	params.Sort = "Id"
	params.Order = "asc"
	data, _ := NotificationCategoryPageList(params)
	return data
}
