package models

import "github.com/astaxie/beego/orm"

type WeChatQueryParam struct {
	BaseQueryParam
}

type WeChat struct {
	Id          int
	WeChatToken string `orm:"column(wechat_token)" form:"WeChatToken"`
	Openid      string
	platform    int
	Nickname    string
	Gender      int
	Country     string
	Province    string
	City        string
	Ip          string
	Avatar      string
	SessionKey  string
	AddTime     int
	UpdateTime  int
}

func init() {
	orm.RegisterModel(new(WeChat))
}

func (u *WeChat) TableName() string {
	return WeChatTBName()
}

func WeChatTBName() string {
	return TableName("wechat")
}

func WeChatOne(id int) (*WeChat, error) {
	o := orm.NewOrm()
	m := WeChat{Id: id}
	err := o.Read(&m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func WeChatPageList(params *WeChatQueryParam) ([]*WeChat, int64) {
	query := orm.NewOrm().QueryTable(WeChatTBName())
	data := make([]*WeChat, 0)

	sort := "Id"
	switch params.Sort {
	case "Id":
		sort = "Id"

	}

	if params.Order == "desc" {
		sort = "-" + sort
	}

	total, _ := query.Count()
	query.OrderBy(sort).Limit(params.Limit, params.Offset).All(&data)
	return data, total
}
